//
//  BFPropertyDeclaration.h
//  BenzeneFoundation
//
//  Created by sodas on 2/24/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface BFPropertyDeclaration : NSObject

+ (NSArray *)propertiesOfClass:(Class)classPtr;
+ (NSArray *)dynamicPropertiesOfClass:(Class)classPtr;
+ (NSArray *)propertiesOfClass:(Class)classPtr dynamic:(BOOL)dynamic synthesize:(BOOL)synthesize;

+ (instancetype)propertyWithDeclaration:(objc_property_t)property;
- (id)initWithDeclaration:(objc_property_t)property;

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly, getter=isReadonly) BOOL readonly;
@property (nonatomic, readonly, getter=isWeak) BOOL weak;
@property (nonatomic, readonly, getter=isCopy) BOOL copy;
@property (nonatomic, readonly) SEL getterSelector;
@property (nonatomic, readonly) SEL setterSelector;
@property (nonatomic, readonly, getter=isDynamic) BOOL dynamic;
@property (nonatomic, readonly) NSString *typeEncoding;

@end
