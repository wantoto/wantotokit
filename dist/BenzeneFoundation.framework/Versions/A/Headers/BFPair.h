//
//  BFPair.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BFPair : NSObject <NSCopying, NSCoding>

+ (instancetype)pairWithObject:(id)first andObject:(id)second;
- (instancetype)initWithObject:(id)first andObject:(id)second;

@property (nonatomic, strong, readwrite) id firstObject;
@property (nonatomic, strong, readwrite) id secondObject;

- (BOOL)isEqualToPair:(BFPair *)pair;

@end

@interface NSDictionary (BFPair)

- (NSArray *)arrayWithKeyAndObjectPairs;

@end

@interface BFPair (BenzeneDictionary)

@property (nonatomic, strong, readonly) id key;
@property (nonatomic, strong, readonly) id value;

@end
