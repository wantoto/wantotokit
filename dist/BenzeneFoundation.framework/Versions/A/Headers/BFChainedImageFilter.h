//
//  BFChainFilter.h
//  BenzeneFoundation
//
//  Created by sodas on 8/5/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <CoreImage/CoreImage.h>

@interface BFChainedImageFilter : CIFilter

+ (instancetype)chainedImageFilterWithFilters:(NSArray *)filters;

@property(nonatomic, strong) NSArray *inputFilters;
@property(nonatomic, strong) CIImage *inputImage;

@end
