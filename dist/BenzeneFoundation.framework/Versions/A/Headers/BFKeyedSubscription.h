//
//  BFKeyedSubscription.h
//  BenzeneFoundation
//
//  Created by sodas on 11/27/13.
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCache (KeyedSubscription)

- (id)objectForKeyedSubscript:(id)key;
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)key;

@end

@interface NSUserDefaults (KeyedSubscription)

- (id)objectForKeyedSubscript:(id)key;
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)key;

@end

@interface NSUbiquitousKeyValueStore (KeyedSubscription)

- (id)objectForKeyedSubscript:(id)key;
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)key;

@end
