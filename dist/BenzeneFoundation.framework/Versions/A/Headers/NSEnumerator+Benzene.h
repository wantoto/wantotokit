//
//  NSEnumerator+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 5/6/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSEnumerator (Benzene)

- (void)enumerateUsingBlock:(void(^)(id obj, NSUInteger idx, BOOL *stop))block;

@end
