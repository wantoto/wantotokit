//
//  UIActionSheet+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 12/11/13.
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActionSheet (Benzene)

+ (instancetype)actionSheetWithTitle:(NSString *)title
                   cancelButtonTitle:(NSString *)cancelButtonTitle
              destructiveButtonTitle:(NSString *)destructiveButtonTitle
                   otherButtonTitles:(NSArray *)otherButtonTitles
                      dismissHandler:(void (^)(NSInteger index, UIActionSheet *actionSheet))handler;

@end
