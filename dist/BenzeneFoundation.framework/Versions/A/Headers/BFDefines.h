//
//  BFDefines.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

#ifndef __BenzeneFoundation
#define __BenzeneFoundation

#define BFBenzeneFoundationVersionMajor 0
#define BFBenzeneFoundationVersionMinor 2
#define BFBenzeneFoundationVersionPatch 0

// Functions

#ifdef __cplusplus
#define BF_EXTERN extern "C" __attribute__((visibility("default")))
#else
#define BF_EXTERN extern __attribute__((visibility("default")))
#endif

#define BF_DEPRECATED __attribute__((deprecated))

#define BF_STATIC_INLINE static inline
#define BF_INLINE inline

// Check ios versions ...
//
// According to Apple's documentation
// It's recommended to check class existence or method respondence first
#define IOS_IS_OLDER_THAN(VER)                                                                                         \
    ([[[UIDevice currentDevice] systemVersion] compare:(VER)options:NSNumericSearch] == NSOrderedAscending)
#define IOS_IS_NEWER_THAN(VER)                                                                                         \
    ([[[UIDevice currentDevice] systemVersion] compare:(VER)options:NSNumericSearch] == NSOrderedDescending)
#define IOS_IS_OLDER_THAN_OR_EQUALS_TO(VER)                                                                            \
    ([[[UIDevice currentDevice] systemVersion] compare:(VER)options:NSNumericSearch] != NSOrderedDescending)
#define IOS_IS_NEWER_THAN_OR_EQUALS_TO(VER)                                                                            \
    ([[[UIDevice currentDevice] systemVersion] compare:(VER)options:NSNumericSearch] != NSOrderedAscending)
#define IOS_IS_OLDER_THAN_7_0 IOS_IS_OLDER_THAN(@"7.0")
#define IOS_IS_OLDER_THAN_6_0 IOS_IS_OLDER_THAN(@"6.0")
#define IOS_IS_OLDER_THAN_5_0 IOS_IS_OLDER_THAN(@"5.0")
#define IOS_IS_NEWER_THAN_7_0 IOS_IS_NEWER_THAN(@"7.0")
#define IOS_IS_NEWER_THAN_6_0 IOS_IS_NEWER_THAN(@"6.0")
#define IOS_IS_NEWER_THAN_5_0 IOS_IS_NEWER_THAN(@"5.0")
#define IOS_IS_OLDER_THAN_OR_EQUALS_TO_7_0 IOS_IS_OLDER_THAN_OR_EQUALS_TO(@"7.0")
#define IOS_IS_OLDER_THAN_OR_EQUALS_TO_6_0 IOS_IS_OLDER_THAN_OR_EQUALS_TO(@"6.0")
#define IOS_IS_OLDER_THAN_OR_EQUALS_TO_5_0 IOS_IS_OLDER_THAN_OR_EQUALS_TO(@"5.0")
#define IOS_IS_NEWER_THAN_OR_EQUALS_TO_7_0 IOS_IS_NEWER_THAN_OR_EQUALS_TO(@"7.0")
#define IOS_IS_NEWER_THAN_OR_EQUALS_TO_6_0 IOS_IS_NEWER_THAN_OR_EQUALS_TO(@"6.0")
#define IOS_IS_NEWER_THAN_OR_EQUALS_TO_5_0 IOS_IS_NEWER_THAN_OR_EQUALS_TO(@"5.0")

// App Info
#define APP_BUNDLE_IDENTIFIER [[NSBundle mainBundle] bundleIdentifier]

// Device type
#define DEVICE_IS_IPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define DEVICE_IS_IPHONE_OR_IPOD_TOUCH ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define DEVICE_IS_TALL_IPHONE_OR_TALL_IPOD_TOUCH                                                                       \
    (DEVICE_IS_IPHONE_OR_IPOD_TOUCH && [[UIScreen mainScreen] bounds].size.height == 568)

// Casting
#define BFObjectCasting(TYPE, VARIABLE) ([(VARIABLE)isKindOfClass:[TYPE class]] ? ((TYPE *)(VARIABLE)) : nil)

// String
#define BFFormatString(format, ...) [NSString stringWithFormat:(format), ##__VA_ARGS__]

#define BFJoinStrings(...) [@[ __VA_ARGS__ ] componentsJoinedByString:@""]

// Random
#define ARC4_RANDOM_MAX ((NSInteger)(pow(2, 32) - 1)) // From man page
BF_STATIC_INLINE u_int32_t BFIntRandomBetween(u_int32_t A, u_int32_t B) {
    return arc4random_uniform(B - A) + A;
}
BF_STATIC_INLINE double BFDoubleRandomBetween(double A, double B) {
    u_int32_t _bound = 4000000000;
    return (arc4random_uniform(_bound) / (double)_bound) * (B-A) + A;
}

// Number
#define BFBoundary(VALUE, MIN_VALUE, MAX_VALUE) (MIN((MAX_VALUE), MAX((MIN_VALUE), (VALUE))))

// Float Equal
BF_STATIC_INLINE BOOL fequalf(float floatA, float floatB) { return fabsf(floatA - floatB) <= FLT_EPSILON; }
BF_STATIC_INLINE BOOL fequal(double doubleA, double doubleB) { return fabs(doubleA - doubleB) <= DBL_EPSILON; }
BF_STATIC_INLINE BOOL fequall(long double longDoubleA, long double longDoubleB) {
    return fabsl(longDoubleA - longDoubleB) <= LDBL_EPSILON;
}

// Float zero
BF_STATIC_INLINE BOOL fzerof(float FLOAT) { return fequalf((FLOAT), .0f); }
BF_STATIC_INLINE BOOL fzero(double DOUBLE) { return fequal((DOUBLE), .0); }
BF_STATIC_INLINE BOOL fzerol(long double LONG_DOUBLE) { return fequall((LONG_DOUBLE), .0l); }

// Angle <-> Degree
BF_STATIC_INLINE CGFloat BFDegreeFromAngle(CGFloat angle) { return angle * 180. / M_PI; }
BF_STATIC_INLINE CGFloat BFAngleFromDegree(CGFloat degree) { return degree * M_PI / 180.; }

BF_STATIC_INLINE CGFloat BFNormalizeDegree180(CGFloat degree) {
    return
    fmod(degree + 360. * (degree<0 ?
                          ((long)(ABS(degree)) / 360 + 1.5):
                          0.5),
         360.)
    - 180.;
}

// Block
#define BFExecuteBlock(BLOCK, ...)                                                                                     \
    if (BLOCK) {                                                                                                       \
        BLOCK(__VA_ARGS__);                                                                                            \
    }
#define BFExecuteAsyncBlockOnDispatchQueue(DISPATCH_QUEUE, BLOCK, ...)                                                 \
    if (BLOCK) {                                                                                                       \
        dispatch_async((DISPATCH_QUEUE), ^{ BLOCK(__VA_ARGS__); });                                                    \
    }
#define BFExecuteAsyncBlockOnMainQueue(BLOCK, ...)                                                                     \
    BFExecuteAsyncBlockOnDispatchQueue(dispatch_get_main_queue(), BLOCK, __VA_ARGS__)
#define BFExecuteBlockOnDispatchQueueAfterDelay(DISPATCH_QUEUE, DELAY, BLOCK, ...)                                     \
    {                                                                                                                  \
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)((DELAY) * NSEC_PER_SEC));                 \
        dispatch_after(popTime, (DISPATCH_QUEUE), ^(void) {BFExecuteBlock(BLOCK, __VA_ARGS__)});                       \
    }
#define BFExecuteBlockOnMainQueueAfterDelay(DELAY, BLOCK, ...)                                                         \
    BFExecuteBlockOnDispatchQueueAfterDelay(dispatch_get_main_queue(), DELAY, BLOCK, __VA_ARGS__)

BF_STATIC_INLINE void BFExecuteBlockOnMainQueueAndWait(dispatch_block_t block) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_group_t group = dispatch_group_create();
        dispatch_group_async(group, dispatch_get_main_queue(), block);
        dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    }
}

BF_STATIC_INLINE void BFExecuteBlockOnBackgroundQueueAndWait(dispatch_queue_t queue, dispatch_block_t block) {
    if ([NSThread isMainThread]) {
        dispatch_group_t group = dispatch_group_create();
        dispatch_group_async(group, queue, block);
        dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    } else {
        block();
    }
}

// Run Loop
#define BFHoldRunLoopWithFlag(RUN_LOOP, MODE, DONE_FLAG, BEFORE_DATE)                                                  \
    while (!(DONE_FLAG)) {                                                                                             \
        @autoreleasepool {                                                                                             \
            [(RUN_LOOP) runMode:(MODE) beforeDate:(BEFORE_DATE)];                                                        \
        }                                                                                                              \
    }

#define BFHoldCurrentRunLoopWithFlag(DONE_FLAG, BEFORE_DATE)                                                           \
    BFHoldRunLoopWithFlag([NSRunLoop currentRunLoop], NSDefaultRunLoopMode, (DONE_FLAG), (BEFORE_DATE))
#define BFHoldCurrentRunLoopWithFlagBeforeTimeInterval(DONE_FLAG, TIME_INTERVAL)                                       \
    BFHoldCurrentRunLoopWithFlag((DONE_FLAG), [NSDate dateWithTimeIntervalSinceNow:(TIME_INTERVAL)])
#define BFHoldCurrentRunLoopWithFlagBeforeShortDistance(DONE_FLAG)                                                     \
    BFHoldCurrentRunLoopWithFlagBeforeTimeInterval(DONE_FLAG, .1)

// Lock Flag
#define BFEntryLock(FLAG, LOCK_TARGET)                                                                                 \
    if (!(FLAG)) {                                                                                                     \
        @synchronized((LOCK_TARGET)) {                                                                                 \
            if (!(FLAG)) {                                                                                             \
                (FLAG) = YES;                                                                                          \
            } else {                                                                                                   \
                return;                                                                                                \
            }                                                                                                          \
        }                                                                                                              \
    } else {                                                                                                           \
        return;                                                                                                        \
    }
#define BFLeavingLock(FLAG) (FLAG) = NO;

// Error
BF_STATIC_INLINE void BFOutputErrorWithUserInfo(NSError *__autoreleasing *error, NSString *domain, NSInteger code,
                                                NSDictionary *userInfo) {
    if (error) {
        *error = [NSError errorWithDomain:domain code:code userInfo:userInfo];
    }
}
BF_STATIC_INLINE void BFOutputError(NSError *__autoreleasing *error, NSString *domain, NSInteger code,
                                    NSString *reason) {
    BFOutputErrorWithUserInfo(error, domain, code, @{NSLocalizedDescriptionKey : reason});
}
BF_STATIC_INLINE void BFOutputErrorWithErrno(NSError *__autoreleasing *error, NSString *domain) {
    BFOutputError(error, domain, errno, @(strerror(errno)));
}

// Free Memory

#define BFFreeCMemoryBlock(BLOCK_PTR)                                                                                  \
    if (BLOCK_PTR)                                                                                                     \
        free(BLOCK_PTR), BLOCK_PTR = NULL;

#define BFDeleteCppObject(OBJ_PTR)                                                                                     \
    if (OBJ_PTR)                                                                                                       \
        delete OBJ_PTR, OBJ_PTR = NULL;

#endif // __BenzeneFoundation
