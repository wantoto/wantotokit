//
//  BFFrameworkBase.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Benzene)

/**
 * Returns the NSBundle object that corresponds to the directory
 * where the Benzene is located.
 *
 * @return     The NSBundle object that corresponds to the directory
 *             where the application executable is located, or nil if
 *             a bundle object could not be created.
 */
+ (NSBundle *)benzeneBundle;

@end
