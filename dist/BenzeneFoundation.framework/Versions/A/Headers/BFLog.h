//
//  BFLog.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BenzeneFoundation/BFDefines.h>

#ifdef DEBUG
#define BFLog(args...) BFDebugLog(__FILE__, __LINE__, __PRETTY_FUNCTION__, args);
#define BFSimpleLog(args...) BFSimpleDebugLog(args)
#else
#define BFLog(x...)
#define BFSimpleLog(args...)
#endif

BF_EXTERN void BFDebugLog(const char *file, int line, const char *func, NSString *format, ...) NS_FORMAT_FUNCTION(4,5);
BF_EXTERN void BFSimpleDebugLog(NSString *format, ...) NS_FORMAT_FUNCTION(1,2);
