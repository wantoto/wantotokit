//
//  NSMetadataQuery+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 4/3/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMetadataQuery (Benzene)

- (void)queryOnceWithCompletionHandler:(void(^)(NSMetadataQuery *metadataQuery))handler;

@end
