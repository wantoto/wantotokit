//
//  NSData+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 8/5/13.
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BenzeneFoundation/BFDefines.h>

BF_EXTERN int BFLoadPartialBytes(void * output, const char *path, size_t location, size_t length);
typedef enum {
    BFLoadPartialBytesSuccess,
    BFLoadPartialBytesCannotOpenFile,
    BFLoadPartialBytesCannotSetFilePosition,
    BFLoadPartialBytesCannotReadFile,
    BFLoadPartialBytesNoOutputBuffer,
} BFLoadPartialBytesResultCode;

@interface NSData (Benzene)

+ (instancetype)dataWithContentsOfFile:(NSString *)path range:(NSRange)range
                                 error:(out NSError *__autoreleasing *)error;

- (instancetype)initWithContentsOfFile:(NSString *)path range:(NSRange)range
                                 error:(out NSError *__autoreleasing *)error;

+ (NSData *)randomDataOfLength:(size_t)length;

- (instancetype)subdataFromIndex:(NSUInteger)index;
- (instancetype)subdataToIndex:(NSUInteger)index;

@end
