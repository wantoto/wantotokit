//
//  BFError.h
//  BenzeneFoundation
//
//  Created by sodas on 8/5/13.
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BenzeneFoundation/BFDefines.h>

BF_EXTERN NSString * const BFBenznenFoundationErrorDomain;
BF_EXTERN NSString * const BFBenznenFoundationIOErrorDomain;

typedef NS_ENUM(NSInteger, BFBenzeneFoundationIOErrorCode) {
    BFBenzeneFoundationMemoryAllocationError,
    BFBenzeneFoundationLowLevelIOError,
};
