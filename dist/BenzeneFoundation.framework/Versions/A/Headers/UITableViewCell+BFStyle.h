//
//  UITableViewCell+BFStyle.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BenzeneFoundation/BFDefines.h>

BF_EXTERN NSString * const BFTableViewCellAttributeBackgroundColor;
BF_EXTERN NSString * const BFTableViewCellAttributeSelectionColor;
BF_EXTERN NSString * const BFTableViewCellAttributeRoundCorners;
BF_EXTERN NSString * const BFTableViewCellAttributeCornerRadius;
BF_EXTERN NSString * const BFTableViewCellAttributeBorderWidth;
BF_EXTERN NSString * const BFTableViewCellAttributeBorderColor;

@interface UITableViewCell (BFStyle)

- (void)formatWithAttributes:(NSDictionary *)attributes;

@end
