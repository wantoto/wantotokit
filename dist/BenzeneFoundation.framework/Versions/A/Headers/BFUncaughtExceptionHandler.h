//
//  BFCrashReporter.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BFUncaughtExceptionHandler : NSObject

+ (BFUncaughtExceptionHandler *)sharedHandler;

- (void)setupWithHandler:(void(^)(NSDictionary *uncaughtExceptionInfo))handler;

- (void)setHandler:(void(^)(NSDictionary *uncaughtExceptionInfo))handler;

@end
