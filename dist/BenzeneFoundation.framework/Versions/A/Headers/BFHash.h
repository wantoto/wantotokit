//
//  BFHash.h
//  BenzeneFoundation
//
//  Created by sodas on 2/25/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BFHash : NSObject

#pragma mark - MD5

+ (NSString *)MD5HashStringWithContentsOfFile:(NSString *)path;
+ (NSString *)MD5HashStringWithWithContentsOfURL:(NSURL *)url;
+ (NSData *)MD5HashDataWithContentsOfFile:(NSString *)path;
+ (NSData *)MD5HashDataWithContentsOfURL:(NSURL *)url;

+ (NSString *)MD5HashString:(NSString *)string;
+ (NSData *)MD5HashData:(NSData *)data;

#pragma mark - SHA256

+ (NSString *)SHA256HashString:(NSString *)string;
+ (NSData *)SHA256HashData:(NSData *)data;

#pragma mark - AES256

+ (NSData *)encryptedAES256Data:(NSData *)data withKey:(NSString *)key
                           salt:(out NSData * __autoreleasing *)salt iv:(out NSData * __autoreleasing *)iv;
+ (NSData *)decryptedAES256Data:(NSData *)data withKey:(NSString *)key salt:(NSData *)salt iv:(NSData *)iv;

+ (NSString *)encryptedAES256String:(NSString *)string withKey:(NSString *)key;
+ (NSString *)decryptedAES256String:(NSString *)string withKey:(NSString *)key;

#pragma mark - Hex Digest

+ (NSString *)hexdigestStringFromData:(NSData *)data;
+ (NSString *)hexdigestStringFromBytes:(unsigned char *)bytes length:(NSUInteger)length;

+ (NSData *)dataFromHexdigestString:(NSString *)string;

@end

@interface NSData (BFHash)

+ (instancetype)dataByEncryptingData:(NSData *)data usingAES256WithKey:(NSString *)key;
+ (instancetype)dataByDecryptingData:(NSData *)data usingAES256WithKey:(NSString *)key;

- (instancetype)encryptedDataUsingAES256WithKey:(NSString *)key;
- (instancetype)decryptedDataUsingAES256WithKey:(NSString *)key;

@end
