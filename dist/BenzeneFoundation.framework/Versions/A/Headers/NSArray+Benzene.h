//
//  NSArray+Benzene.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Benzene)

- (instancetype)arrayByReversingArray;

- (instancetype)subarrayFromIndex:(NSUInteger)from;
- (instancetype)subarrayToIndex:(NSUInteger)to;

- (instancetype)filteredArrayByKeypath:(NSString *)keyPath;
- (instancetype)filteredArrayByBooleanKeypath:(NSString *)keyPath;
- (instancetype)filteredArrayByKeypath:(NSString *)keyPath withValue:(id)value;

- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

+ (void)enumerateObjectsInArrays:(NSArray *)arrayList usingBlock:(void (^)(id, NSUInteger, BOOL *))block;

@end

@interface NSMutableArray (Benzene)

- (void)reverseArray;

@end