Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.name                = "BenzeneFoundation"
  s.version             = "0.2.0"
  s.summary             = "Extension of UIKit and Foundation"
  s.homepage            = "https://bitbucket.org/wantoto/wantotokit"

  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.license             = "Commercial"

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.author              = { "sodastsai" => "sodas2002@gmail.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.platform            = :ios, "7.0"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  # This is a podspec for local pod

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.vendored_frameworks = "BenzeneFoundation.framework"
  s.public_header_files = "BenzeneFoundation.framework/**/*.h"

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.resource            = "BenzeneFoundation.bundle"

  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.frameworks          = "Foundation", "UIKit", "CoreGraphics"

  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.preserve_paths      = "BenzeneFoundation.framework", "BenzeneFoundation.bundle"
  s.requires_arc        = true
  s.xcconfig            = { "OTHER_LDFLAGS" => "-ObjC" }

end
