Pod::Spec.new do |s|

  # -- Package Information  ------------------------------------------------------------------------------------------ #

  s.name = "BenzeneFoundation"
  s.version = "0.5.42"
  s.summary = "Extension of UIKit and Foundation"
  s.description = "Collections of extensions/utilities from Wantoto Inc. for UIKit and Foundation"
  s.homepage = "https://bitbucket.org/wantoto/wantotokit"
  s.license = "BSD"
  s.author = {"sodastsai" => "sodas@wantoto.com"}
  s.source = {:git => "https://bitbucket.org/wantoto/wantotokit.git", :tag => "v#{s.version.to_s}"}
  s.ios.deployment_target = '7.0'
  s.osx.deployment_target = '10.9'

  # -- Subspecs ------------------------------------------------------------------------------------------------------ #

  s.subspec "Foundation" do |fs|
    fs.ios.deployment_target = '7.0'
    fs.osx.deployment_target = '10.9'

    fs.requires_arc = true
    fs.xcconfig = { "OTHER_LDFLAGS" => "-ObjC" }

    fs.source_files = "BenzeneFoundation/BenzeneFoundation/**/*.{h,m}"
    fs.public_header_files = "BenzeneFoundation/BenzeneFoundation/**/*.h"
    fs.private_header_files = "BenzeneFoundation/BenzeneFoundation/**/*_Internal.h"
    fs.frameworks = "Foundation", "CoreGraphics"
    fs.dependency "libextobjc", '~> 0.4'
  end

  s.subspec "UIKit" do |us|
    us.ios.deployment_target = '7.0'

    us.requires_arc = true
    us.xcconfig = { "OTHER_LDFLAGS" => "-ObjC" }

    us.source_files = "BenzeneFoundation/BenzeneUIKit/**/*.{h,m}"
    us.public_header_files = "BenzeneFoundation/BenzeneUIKit/**/*.h"
    us.resource_bundles = {
      "BenzeneUIKit" => [
        "BenzeneFoundation/BenzeneUIKit/**/*.{png,pdf,lproj,json,xib,storyboard}"
      ]
    }
    us.frameworks = "Foundation", "CoreGraphics", "UIKit"
    us.dependency "BenzeneFoundation/Foundation"
    us.dependency "libextobjc", '~> 0.4'
  end

end
