//
//  BDStringTests.m
//  Benzene Demo
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//  * Neither the name of the Wantoto Inc. nor the
//    names of its contributors may be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Wantoto Inc. BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <BenzeneFoundation/NSString+Benzene.h>

@interface BDStringTests : XCTestCase

@end

@implementation BDStringTests

- (void)testCGFloatValue {
    XCTAssertEqualWithAccuracy([@"42.3" CGFloatValue], 42.3, DBL_EPSILON);
}

- (void)testRangeOfWholeString {
    XCTAssertEqualObjects([NSValue valueWithRange:@"ABC".rangeOfWholeString],
                          [NSValue valueWithRange:NSMakeRange(0, 3)]);
    XCTAssertEqualObjects([NSValue valueWithRange:@"xyz123".rangeOfWholeString],
                          [NSValue valueWithRange:NSMakeRange(0, 6)]);
}

- (void)testContainsString {
    XCTAssertTrue([@"ABC" containsString:@"BC"]);
    XCTAssertFalse([@"ABC" containsString:@"X"]);
}

- (void)testUppercaseInRange {
    XCTAssertEqualObjects([@"abcde" stringByUppercaseInRange:NSMakeRange(1, 2)], @"aBCde");
    XCTAssertEqualObjects([@"ABCDE" stringByUppercaseInRange:NSMakeRange(1, 2)], @"ABCDE");
    XCTAssertEqualObjects([@"abcde" stringByUppercaseInRange:NSMakeRange(0, 5)], @"ABCDE");
}

- (void)testLowercaseInRange {
    XCTAssertEqualObjects([@"abcde" stringByLowercaseInRange:NSMakeRange(1, 2)], @"abcde");
    XCTAssertEqualObjects([@"ABCDE" stringByLowercaseInRange:NSMakeRange(1, 2)], @"AbcDE");
    XCTAssertEqualObjects([@"ABCDE" stringByLowercaseInRange:NSMakeRange(0, 5)], @"abcde");
}

- (void)testNumberString {
    XCTAssertTrue(@"42".numberString, @"\"42\" is indeed a number string.");
    XCTAssertTrue(@"42.21".numberString, @"\"42.21\" is indeed a number string.");
    XCTAssertTrue(@"-31".numberString, @"\"-31\" is indeed a number string.");
    XCTAssertTrue(@"6.24e18".numberString, @"\"6.23e18\" is indeed a number string.");
    XCTAssertTrue(@"6.02E+23".numberString, @"\"6.02e+23\" is indeed a number string.");
    XCTAssertTrue(@"6.02e-23".numberString, @"\"6.02e+23\" is indeed a number string.");
    XCTAssertFalse(@"A2".numberString, @"\"A2\" is not a number string.");
    XCTAssertFalse(@"Hello World!".numberString, @"\"Hello World!\" is not a number string.");
    XCTAssertFalse(@"Hello 3831.25 World!".numberString, @"\"Hello 3831.25 World!\" is not a number string.");
}

- (void)testPerformanceNumberString {
    [self measureBlock:^{
        [@"42" isNumberString];
    }];
}

- (void)testEnumerateCharacterSet {
    {
        NSUInteger __block count = 0;
        [@"abc%d%f%" enumerateCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"%"]
                                    withBlock:^(unichar character, NSUInteger position, BOOL *stop) {
                                        XCTAssertEqual(character, '%');
                                        if (count == 0) {
                                            XCTAssertEqual(position, 3);
                                        } else if (count == 1) {
                                            XCTAssertEqual(position, 5);
                                        } else if (count == 2) {
                                            XCTAssertEqual(position, 7);
                                        }
                                        count++;
                                    }];
        XCTAssertEqual(count, 3, @"Should found 2 '%%'.");
    }

    {
        NSUInteger __block count = 0;
        [@"ab:c%d%:f" enumerateCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"%:"]
                                    withBlock:^(unichar character, NSUInteger position, BOOL *stop) {
                                        if (count == 0 || count == 3) {
                                            XCTAssertEqual(character, ':');
                                            if (count == 0) {
                                                XCTAssertEqual(position, 2);
                                            } else {
                                                XCTAssertEqual(position, 7);
                                            }
                                        } else if (count == 1 || count == 2) {
                                            XCTAssertEqual(character, '%');
                                            if (count == 1) {
                                                XCTAssertEqual(position, 4);
                                            } else {
                                                XCTAssertEqual(position, 6);
                                            }
                                        }
                                        count++;
                                    }];
        XCTAssertEqual(count, 4);
    }

    {
        NSUInteger __block count = 0;
        [@"ab:c%d%:f" enumerateCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"%:"]
                                        options:0
                                          range:NSMakeRange(0, 3)
                                      withBlock:^(unichar character, NSUInteger position, BOOL *stop) {
                                          count++;
                                      }];
        XCTAssertEqual(count, 1);
    }

    {
        NSUInteger __block count = 0;
        [@"ab:c%d%:f" enumerateCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"%:"]
                                        options:0
                                          range:NSMakeRange(2, 5)
                                      withBlock:^(unichar character, NSUInteger position, BOOL *stop) {
                                          count++;
                                      }];
        XCTAssertEqual(count, 3);
    }

    {
        NSUInteger __block count = 0;
        [@"ab:c%d%:f" enumerateCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"%:"]
                                        options:0
                                          range:NSMakeRange(2, 5)
                                      withBlock:^(unichar character, NSUInteger position, BOOL *stop) {
                                          *stop = ++count == 2;
                                      }];
        XCTAssertEqual(count, 2);
    }

    {
        NSUInteger __block count = 0;
        [@"123456" enumerateCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"%:"]
                                      withBlock:^(unichar character, NSUInteger position, BOOL *stop) {
                                          XCTFail("The enumeration block should not be get called.");
                                      }];
        XCTAssertEqual(count, 0);
    }
}

- (void)testReplacingCharacters {
    XCTAssertEqualObjects([@"a\nb\r\nc" stringByReplacingCharactersFromSet:[NSCharacterSet newlineCharacterSet]
                                                                withString:@""],
                          @"abc");

    XCTAssertEqualObjects([@"a\nb\r\nc" stringByReplacingCharactersFromSet:[NSCharacterSet newlineCharacterSet]
                                                                withString:@"<br>"],
                          @"a<br>b<br><br>c");

    XCTAssertEqualObjects([@"a\nb\t\nc" stringByReplacingCharactersFromSet:[NSCharacterSet newlineCharacterSet]
                                                                withString:@"1"],
                          @"a1b\t1c");
}

- (void)testEnumerateOccurrencesOfString {
    {
        NSUInteger __block count = 0;
        [@"ABCBCxddbc12BC" enumerateOccurrencesOfString:@"BC" withBlock:^(NSUInteger position, BOOL *stop) {
            switch (count++) {
                case 0:
                    XCTAssertEqual(position, 1);
                    break;
                case 1:
                    XCTAssertEqual(position, 3);
                    break;
                case 2:
                    XCTAssertEqual(position, 12);
                    break;
                default:
                    break;
            }
        }];
        XCTAssertEqual(count, 3);
    }

    {
        NSUInteger __block count = 0;
        [@"ABCBCxddbc12BC" enumerateOccurrencesOfString:@"BC" withBlock:^(NSUInteger position, BOOL *stop) {
            switch (count++) {
                case 0:
                    XCTAssertEqual(position, 1);
                    break;
                case 1:
                    XCTAssertEqual(position, 3);
                    *stop = YES;
                    break;
                default:
                    break;
            }
        }];
        XCTAssertEqual(count, 2);
    }

    {
        NSUInteger __block count = 0;
        [@"ABCBCxddbc12BC" enumerateOccurrencesOfString:@"yz" withBlock:^(NSUInteger position, BOOL *stop) {
            XCTFail("The enumeration block should not be called.");
        }];
        XCTAssertEqual(count, 0);
    }
}

- (void)testStringWithFormatAndArgumentsArray {
    XCTAssertEqualObjects([NSString stringWithFormat:@"Answer=%@" argumentsArray:@[@42]], @"Answer=42");

    XCTAssertEqualObjects([NSString stringWithFormat:@"Answer=%@, %%" argumentsArray:@[@42]], @"Answer=42, %");

    XCTAssertThrows([NSString stringWithFormat:@"Answer=%@, %@" argumentsArray:@[@42]]);

    NSArray *arg = @[@42, @43];
    XCTAssertEqualObjects([NSString stringWithFormat:@"Answer=%@" argumentsArray:arg], @"Answer=42");

    XCTAssertThrows([NSString stringWithFormat:@"Answer=%ld" argumentsArray:@[@42]]);
}

@end
