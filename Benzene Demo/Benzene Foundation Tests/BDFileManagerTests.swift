//
//  BDFileManagerTests.swift
//  Benzene Demo
//
//  Created by sodas on 6/11/15.
//  Copyright (c) 2015 Benzene. All rights reserved.
//

import UIKit
import XCTest

class BDFileManagerTests: XCTestCase {

    lazy var bundle: NSBundle = { [unowned self] in
        return NSBundle(forClass: self.dynamicType)
    }()

    // MARK: - zip

    func testZip() {
        if let validPath = self.bundle.pathForResource("valid-zip", ofType: "zip") {
            XCTAssertTrue(NSFileManager.defaultManager().isZipFileAtPath(validPath),
                "\(validPath) is a valid zip")
        } else {
            XCTFail("Failed to find a valid zip file for testing")
        }

        if let validPath = self.bundle.pathForResource("invalid-zip", ofType: "zip") {
            XCTAssertFalse(NSFileManager.defaultManager().isZipFileAtPath(validPath),
                "\(validPath) is an invalid zip")
        } else {
            XCTFail("Failed to find an invalid zip file for testing")
        }
    }

    // MARK: - atmomic

    func testAtomicOperationFailed() {
        NSFileManager.defaultManager().executeBlockWithinTemporaryDirectory { (tempDirPath: String) in
            let sourceFileURL: NSURL = NSURL(fileURLWithPath: tempDirPath).URLByAppendingPathComponent("1234")
            let sourceData = NSData.randomDataOfLength(32)
            sourceData.writeToURL(sourceFileURL, atomically: true)

            try! NSFileManager.defaultManager().performAtomicOperationForURL(sourceFileURL) {
                NSData.randomDataOfLength(64).writeToURL(sourceFileURL, atomically: true)
                return false
            }

            let finalData = NSData(contentsOfURL: sourceFileURL)
            XCTAssertEqual(sourceData, finalData)
        }
    }

    func testAtomicOperationSuccess() {
        NSFileManager.defaultManager().executeBlockWithinTemporaryDirectory { (tempDirPath: String) in
            let sourceFileURL: NSURL = NSURL(fileURLWithPath: tempDirPath).URLByAppendingPathComponent("1234")
            let sourceData = NSData.randomDataOfLength(32)
            sourceData.writeToURL(sourceFileURL, atomically: true)

            var updatedData: NSData!
            try! NSFileManager.defaultManager().performAtomicOperationForURL(sourceFileURL) {
                updatedData = NSData.randomDataOfLength(64)
                updatedData.writeToURL(sourceFileURL, atomically: true)
                return true
            }

            let finalData = NSData(contentsOfURL: sourceFileURL)
            XCTAssertEqual(updatedData, finalData)
            XCTAssertNotEqual(sourceData, finalData)
        }
    }

}
