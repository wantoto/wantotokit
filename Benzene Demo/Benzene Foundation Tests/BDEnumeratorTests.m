//
//  BDEnumeratorTests.m
//  Benzene Demo
//
//  Created by sodas on 9/14/15.
//  Copyright © 2015 Benzene. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/BenzeneFoundation.h>

@interface BDEnumeratorTests : XCTestCase

@end

@implementation BDEnumeratorTests

- (void)testEnumerateWithBlock {
    NSEnumerator<NSNumber *> *enumerator = @[@1, @4, @9, @16, @25, @36, @49, @64, @81, @100].objectEnumerator;
    [enumerator enumerateUsingBlock:^(NSNumber *number, NSUInteger idx, BOOL * _Nonnull stop) {
        XCTAssertEqual(number.integerValue, (idx+1)*(idx+1));
    }];
}

- (void)testChainedArrays {
    NSArray<NSNumber *> *smallNumbers = @[@1, @2, @3];
    NSArray<NSNumber *> *largeNumbers = @[@4, @5];
    NSEnumerator<NSNumber *><BFChainedArrayEnumerator> *enumerator = [NSEnumerator enumeratorByChainingArrays:@[
        smallNumbers,
        largeNumbers,
    ]];

    NSInteger currentValue = 1;
    XCTAssertEqual(enumerator.nextObject.integerValue, currentValue++);
    for (NSNumber *number in enumerator) {
        XCTAssertEqual(number.integerValue, currentValue++);
        if (number.integerValue <= 3) {
            XCTAssertEqual(enumerator.currentArray, smallNumbers);
        } else {
            XCTAssertEqual(enumerator.currentArray, largeNumbers);
        }
    }
    XCTAssertEqual(currentValue, 6);
}

- (void)testChainedArraysWithEmptyArrays {
    NSEnumerator<NSNumber *> *enumerator = [NSEnumerator<NSNumber *> enumeratorByChainingArrays:@[
        @[@1, @2, @3],
        @[],
        @[@4, @5],
        @[],
        @[@6],
    ]];
    
    NSInteger currentValue = 1;
    XCTAssertEqual(enumerator.nextObject.integerValue, currentValue++);
    for (NSNumber *number in enumerator) {
        XCTAssertEqual(number.integerValue, currentValue++);
    }
    XCTAssertEqual(currentValue, 7);
}

- (void)testChainedEnumerators {
    NSEnumerator<NSNumber *> *enumerator1 = @{@"answer": @42}.objectEnumerator;
    NSEnumerator<NSNumber *> *enumerator2 = @{@"answer2": @43, @"answer3": @44}.objectEnumerator;
    NSEnumerator<NSNumber *><BFChainedEnumerator> *enumerator = [NSEnumerator enumeratorByChainingEnumerators:@[
        enumerator1,
        enumerator2,
        @[].objectEnumerator,
    ]];

    NSInteger enumeratorCalledCount = 0;
    NSArray *expectedItems = @[@42, @43, @44];
    for (NSNumber *number in enumerator) {
        if (number.integerValue <= 42) {
            XCTAssertEqual(enumerator.currentEnumerator, enumerator1);
        } else if (number.integerValue <= 44) {
            XCTAssertEqual(enumerator.currentEnumerator, enumerator2);
        }
        XCTAssertEqualObjects(expectedItems[enumeratorCalledCount++], number);
    }
    XCTAssertEqual(enumeratorCalledCount, 3);
}

- (void)testKeyPathEnumerator {
    NSArray<NSString *> *hostNames = @[
        @"Peter",
        @"Amy",
        @"Elmer",
        @"Claire",
        @"Bill",
        @"Eric",
        @"Eva",
    ];

    NSMutableArray<NSDictionary *> *source = [NSMutableArray arrayWithCapacity:hostNames.count];
    [hostNames enumerateObjectsUsingBlock:^(NSString *hostName, NSUInteger idx, BOOL *stop) {
        [source addObject:@{@"host": @{@"name": hostName}}];
    }];

    NSEnumerator<NSString *> *hostNameEnumerator =
        (NSEnumerator<NSString *> *)[source.objectEnumerator enumeratorWithKeyPathOfObjects:@"host.name"];

    [hostNameEnumerator enumerateUsingBlock:^(NSString *hostName, NSUInteger idx, BOOL *stop) {
        XCTAssertEqualObjects(hostName, hostNames[idx]);
    }];
}

- (void)testBlockEnumerator {
    NSUInteger __block i = 0;
    NSEnumerator<NSNumber *> *enumerator = [NSEnumerator<NSNumber *> enumeratorWithBlock:^NSNumber * _Nullable{
        return ((i += 2) <= 10) ? @(i) : nil;
    }];
    XCTAssertEqualObjects(enumerator.allObjects, (@[@2, @4, @6, @8, @10]));
}

- (NSEnumerator<NSEnumerator<NSNumber *> *> *)_flattingEnumeratorSource {
    return @[
        @[@0].objectEnumerator,
        @[@0, @2].objectEnumerator,
        @[@0, @2, @4].objectEnumerator,
        @[@0, @2, @4, @6].objectEnumerator,
        @[@0, @2, @4, @6, @8].objectEnumerator,
    ].objectEnumerator;
}

- (void)testFlattingEnumerator {
    NSMutableArray<NSNumber *> *expected = [NSMutableArray array];
    [[self _flattingEnumeratorSource]
     enumerateUsingBlock:^(NSEnumerator<NSNumber *> *enumerator, NSUInteger idx, BOOL *stop) {
         [enumerator enumerateUsingBlock:^(NSNumber *number, NSUInteger idx, BOOL *stop) {
             [expected addObject:number];
         }];
     }];
    
    NSEnumerator<NSNumber *> *enumeratorToTest = [NSEnumerator<NSNumber *>
                                                  enumeratorByFlattingEnumerator:[self _flattingEnumeratorSource]];

    XCTAssertEqualObjects(expected, enumeratorToTest.allObjects);
}

- (void)testPredicateEnumerator {
    NSArray<NSNumber *> *numbers = @[@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12];
    NSArray<NSNumber *> *expected = @[@0, @3, @6, @9, @12];
    NSEnumerator<NSNumber *> *enumerator =
    [numbers.objectEnumerator
     enumeratorFilteredByPredicate:[NSPredicate predicateWithBlock:^BOOL(NSNumber *number,
                                                                         NSDictionary<NSString *, id> *bindings) {
        return number.integerValue % 3 == 0;
    }]];

    XCTAssertEqualObjects(enumerator.allObjects, expected);
}

- (void)testObjectBlockEnumerator {
    NSArray<NSNumber *> *source = @[@0, @3, @6, @9, @12];
    NSEnumerator<NSNumber *> *enumerator =
    [source.objectEnumerator enumeratorWithObjectBlock:^NSNumber * _Nonnull(NSNumber * _Nonnull obj) {
        return @(obj.integerValue * obj.integerValue);
    }];
    NSArray<NSNumber *> *expected = @[@0, @9, @36, @81, @144];
    XCTAssertEqualObjects(enumerator.allObjects, expected);
}

@end
