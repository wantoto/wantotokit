//
//  BFPairTests.swift
//  Benzene Demo
//
//  Created by sodas on 11/11/15.
//  Copyright © 2015 Benzene. All rights reserved.
//

import XCTest

class BFPairTests: XCTestCase {
    
    func testPairCreation() {
        let pair = BFPair(object: "answer", andObject: 42)

        XCTAssertEqual(pair.firstObject as? String, "answer")
        XCTAssertEqual(pair.secondObject as? Int, 42)
    }

    func testMutablePairCreation() {
        let pair = BFMutablePair(object: "answer", andObject: 42)

        XCTAssertEqual(pair.firstObject as? String, "answer")
        XCTAssertEqual(pair.secondObject as? Int, 42)

        pair.firstObject = "message"
        pair.secondObject = 24

        XCTAssertEqual(pair.firstObject as? String, "message")
        XCTAssertEqual(pair.secondObject as? Int, 24)
    }

    func testDictionaryInterface() {
        let dict: NSDictionary = ["answer": 42, "message": 24]
        let expectation: [[AnyObject]] = [
            ["answer", 42],
            ["message", 24]
        ]
        for (index, pair) in dict.arrayWithKeyAndObjectPairs.enumerate() {
            XCTAssertEqual(pair.firstObject as? String, expectation[index][0] as? String)
            XCTAssertEqual(pair.key as? String, expectation[index][0] as? String)
            XCTAssertEqual(pair.secondObject as? Int, expectation[index][1] as? Int)
            XCTAssertEqual(pair.object as? Int, expectation[index][1] as? Int)
        }
    }

    func testSimpleCopying() {
        let pair = BFPair(object: "answer", andObject: 42)
        let copiedPair = pair.copy() as! BFPair

        XCTAssertEqual(pair, copiedPair)
        XCTAssertEqual(copiedPair.firstObject as? String, "answer")
        XCTAssertEqual(copiedPair.secondObject as? Int, 42)
        XCTAssertTrue(pair === copiedPair)
    }

    func testMutableToImmutableCopying() {
        let mutablePair = BFMutablePair(object: "answer", andObject: 42)
        let copiedPair = mutablePair.copy() as! BFPair

        XCTAssertEqual(mutablePair, copiedPair)
        XCTAssertEqual(copiedPair.firstObject as? String, "answer")
        XCTAssertEqual(copiedPair.secondObject as? Int, 42)
        XCTAssertTrue(mutablePair !== copiedPair)

        mutablePair.firstObject = "message"
        mutablePair.secondObject = 24

        XCTAssertEqual(copiedPair.firstObject as? String, "answer")
        XCTAssertEqual(copiedPair.secondObject as? Int, 42)
    }

    func testImmutableToMutableCopying() {
        let pair = BFPair(object: "answer", andObject: 42)
        let copiedMutablePair = pair.mutableCopy() as! BFMutablePair

        XCTAssertEqual(pair, copiedMutablePair)
        XCTAssertEqual(copiedMutablePair.firstObject as? String, "answer")
        XCTAssertEqual(copiedMutablePair.secondObject as? Int, 42)
        XCTAssertTrue(pair !== copiedMutablePair)

        copiedMutablePair.firstObject = "message"
        copiedMutablePair.secondObject = 24

        XCTAssertEqual(copiedMutablePair.firstObject as? String, "message")
        XCTAssertEqual(copiedMutablePair.secondObject as? Int, 24)
        XCTAssertEqual(pair.firstObject as? String, "answer")
        XCTAssertEqual(pair.secondObject as? Int, 42)
    }

    func testMutableToMutableCopying() {
        let mutablePair = BFMutablePair(object: "answer", andObject: 42)
        let copiedMutablePair = mutablePair.mutableCopy() as! BFMutablePair

        XCTAssertEqual(mutablePair, copiedMutablePair)
        XCTAssertEqual(copiedMutablePair.firstObject as? String, "answer")
        XCTAssertEqual(copiedMutablePair.secondObject as? Int, 42)
        XCTAssertTrue(mutablePair !== copiedMutablePair)

        mutablePair.firstObject = "XD"
        copiedMutablePair.secondObject = 512

        XCTAssertEqual(mutablePair.firstObject as? String, "XD")
        XCTAssertEqual(mutablePair.secondObject as? Int, 42)
        XCTAssertEqual(copiedMutablePair.firstObject as? String, "answer")
        XCTAssertEqual(copiedMutablePair.secondObject as? Int, 512)
    }

    func testCoding() {
        let pair = BFPair(object: "answer", andObject: 42)

        let codingData = NSKeyedArchiver.archivedDataWithRootObject(pair)
        let codingPair = NSKeyedUnarchiver.unarchiveObjectWithData(codingData) as! BFPair

        XCTAssertEqual(pair, codingPair)
        XCTAssertEqual(codingPair.firstObject as? String, "answer")
        XCTAssertEqual(codingPair.secondObject as? Int, 42)
        XCTAssertTrue(pair !== codingPair)
    }
    
}
