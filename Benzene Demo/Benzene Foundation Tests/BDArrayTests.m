//
//  BDArrayTests.m
//  Benzene Demo
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//  * Neither the name of the Wantoto Inc. nor the
//    names of its contributors may be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Wantoto Inc. BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/NSArray+Benzene.h>

@interface BDArrayTests : XCTestCase

@end

@implementation BDArrayTests

- (void)testIndexOfLastSameObjectFromHeadWithArray {
    XCTAssertEqual([(@[@1, @2, @4, @8]) indexOfLastSameObjectFromHeadWithArray:(@[@1, @2, @3, @5])], 1);
    XCTAssertEqual([(@[@1, @2, @4, @8]) indexOfLastSameObjectFromHeadWithArray:(@[@1, @2, @4, @8])], 3);
    XCTAssertEqual([(@[@1, @2, @4, @8]) indexOfLastSameObjectFromHeadWithArray:(@[@9, @2, @3, @5])], -1);
}

- (void)testReverseArray {
    XCTAssertEqualObjects((@[@4, @3, @2, @1]), (@[@1, @2, @3, @4]).arrayByReversingArray);
    XCTAssertEqualObjects((@[@5, @4, @3, @2, @1]), (@[@1, @2, @3, @4, @5]).arrayByReversingArray);
}

- (void)testSubarrayToIndex {
    XCTAssertEqualObjects((@[@1, @2, @3]), [(@[@1, @2, @3, @4]) subarrayToIndex:3]);
    XCTAssertEqualObjects((@[@1, @2, @3, @4]), [(@[@1, @2, @3, @4]) subarrayToIndex:6]);
}

- (void)testSubarrayFromIndex {
    XCTAssertEqualObjects((@[@3, @4]), [(@[@1, @2, @3, @4]) subarrayFromIndex:2]);
    XCTAssertEqualObjects((@[]), [(@[@1, @2, @3, @4]) subarrayFromIndex:6]);
}

- (void)testArraySlicing {
    NSArray *originalArray = @[@1, @2, @3, @4, @5, @6, @7, @8, @9, @10];
    NSMutableArray *visitedItems = [NSMutableArray arrayWithCapacity:originalArray.count];

    NSUInteger __block currentBucketIndex = 0;
    [originalArray
     enumerateArrayBySlicingIntoBucketSize:3
     withBlock:^(NSArray *subArray, NSUInteger bucketIndex, BOOL *stop) {
         XCTAssertEqual(currentBucketIndex++, bucketIndex);
         XCTAssertEqual(subArray.count, bucketIndex != 3 ? 3 : 1);
         [visitedItems addObjectsFromArray:subArray];
    }];
    XCTAssertEqual(currentBucketIndex, 4);
    XCTAssertEqualObjects(originalArray, visitedItems);
}

- (void)testArraySlicingStop {
    NSArray *originalArray = @[@1, @2, @3, @4, @5, @6, @7, @8, @9, @10];
    NSMutableArray *visitedItems = [NSMutableArray arrayWithCapacity:originalArray.count];

    NSUInteger __block currentBucketIndex = 0;
    [originalArray
     enumerateArrayBySlicingIntoBucketSize:3
     withBlock:^(NSArray *subArray, NSUInteger bucketIndex, BOOL *stop) {
         XCTAssertEqual(currentBucketIndex++, bucketIndex);
         [visitedItems addObjectsFromArray:subArray];

         *stop = bucketIndex == 1;
     }];
    XCTAssertEqual(currentBucketIndex, 2);
    XCTAssertEqualObjects((@[@1, @2, @3, @4, @5, @6]), visitedItems);
}

@end
