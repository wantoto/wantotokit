//
//  BDCopyingTests.m
//  Benzene Demo
//
//  Created by sodas on 6/10/15.
//  Copyright (c) 2015 Benzene. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/BenzeneFoundation.h>

@interface CopiableObject : NSObject <NSCopying>

@property (nonatomic, strong) NSArray *array;

@end

@implementation CopiableObject

- (instancetype)init {
    if (self = [super init]) {
        self.array = @[ [@"string" mutableCopy], ];
    }
    return self;
}

- (instancetype)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] init];
}

@end

@interface BDCopyingTests : XCTestCase

@end

// NOTE: `copy` of NSNumber, NSString (or other immutables) is just `retain`

NSString *const BDCopyingTestsDictStringKey = @"string";
NSString *const BDCopyingTestsDictNumberKey = @"nunber";
NSString *const BDCopyingTestsDictObjectKey = @"object";
NSString *const BDCopyingTestsDictDictKey = @"dict";
NSString *const BDCopyingTestsDictArrayKey = @"array";

@implementation BDCopyingTests

- (void)testCopiableObject {
    CopiableObject *sourceObject = [[CopiableObject alloc] init];
    CopiableObject *copiedObject = [sourceObject copy];
    XCTAssertNotEqual(sourceObject, copiedObject);
    XCTAssertNotEqual(sourceObject.array, copiedObject.array);
    XCTAssertNotEqual(sourceObject.array[0], copiedObject.array[0]);
}

- (void)testDeepCopy {
    NSArray *originalArray = @[
        [@"Answer" mutableCopy],
        @42,
        [[CopiableObject alloc] init],
        @[
            [@"String" mutableCopy],
            [[CopiableObject alloc] init],
            [NSSet setWithObject:[[CopiableObject alloc] init]],
        ],
        @{
            BDCopyingTestsDictObjectKey: [[CopiableObject alloc] init],
            BDCopyingTestsDictArrayKey: @[ [@"String" mutableCopy] ],
        },
        [NSSet setWithObjects:
         [[CopiableObject alloc] init],
         @[ [@"String" mutableCopy] ],
         @{ BDCopyingTestsDictObjectKey: [[CopiableObject alloc] init], },
         nil],
        [NSOrderedSet orderedSetWithObjects:[@"String" mutableCopy], [[CopiableObject alloc] init], nil],
    ];
    NSArray *deepCopiedArray = [originalArray deepCopy];
    XCTAssertNotEqual(originalArray, deepCopiedArray);
    XCTAssertNotEqual(originalArray[0], deepCopiedArray[0]);
    XCTAssertEqual(originalArray[1], deepCopiedArray[1]);  // NSNumber
    XCTAssertNotEqual(originalArray[2], deepCopiedArray[2]);

    XCTAssertNotEqual(originalArray[3], deepCopiedArray[3]);
    XCTAssertNotEqual(originalArray[3][0], deepCopiedArray[3][0]);
    XCTAssertNotEqual(originalArray[3][1], deepCopiedArray[3][1]);

    XCTAssertNotEqual(originalArray[4][BDCopyingTestsDictObjectKey], deepCopiedArray[4][BDCopyingTestsDictObjectKey]);
    XCTAssertNotEqual(originalArray[4][BDCopyingTestsDictArrayKey], deepCopiedArray[4][BDCopyingTestsDictArrayKey]);
    XCTAssertNotEqual(originalArray[4][BDCopyingTestsDictArrayKey][0],
                      deepCopiedArray[4][BDCopyingTestsDictArrayKey][0]);

    {
        NSSet *originalSet = originalArray[5];
        NSSet *deepCopiedSet = deepCopiedArray[5];
        NSComparator classNameComparator = ^NSComparisonResult(id obj1, id obj2) {
            return [NSStringFromClass([obj1 class]) compare:NSStringFromClass([obj2 class])];
        };
        NSArray *sortedOriginalSet = [[originalSet allObjects] sortedArrayUsingComparator:classNameComparator];
        NSArray *sortedDeepCopiedSet = [[deepCopiedSet allObjects] sortedArrayUsingComparator:classNameComparator];
        XCTAssertNotEqual(originalSet, deepCopiedSet);
        XCTAssertNotEqual(sortedOriginalSet[0], sortedDeepCopiedSet[0]);
        XCTAssertNotEqual(sortedOriginalSet[1], sortedDeepCopiedSet[1]);
        XCTAssertNotEqual(sortedOriginalSet[2], sortedDeepCopiedSet[2]);
        XCTAssertNotEqual(sortedOriginalSet[2][BDCopyingTestsDictObjectKey],
                          sortedDeepCopiedSet[2][BDCopyingTestsDictObjectKey]);
    }

    XCTAssertNotEqual(originalArray[6], deepCopiedArray[6]);
    XCTAssertNotEqual(originalArray[6][0], deepCopiedArray[6][0]);
    XCTAssertNotEqual(originalArray[6][1], deepCopiedArray[6][1]);
}

- (void)testDeepMutableCopy {
    XCTAssertTrue([[@[] deepMutableCopy] isKindOfClass:[NSMutableArray class]]);
    XCTAssertTrue([[@[ @{} ] deepMutableCopy][0] isKindOfClass:[NSMutableDictionary class]]);
}

@end
