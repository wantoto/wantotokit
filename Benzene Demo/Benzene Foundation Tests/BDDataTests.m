//
//  BDDataTests.m
//  Benzene Demo
//
//  Created by sodas on 1/22/16.
//  Copyright © 2016 Benzene. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/BenzeneFoundation.h>

@interface BDDataTests : XCTestCase

@end

@implementation BDDataTests

- (void)testDataBytesCreation {
    NSData *data = [NSData dataWithLength:3 bytes:'\x01', '\x02', '\x03'];
    XCTAssertTrue(memcmp(data.bytes, "\x01\x02\x03", 3) == 0);
}

- (void)testDataEqualToBytes {
    NSData *data = [NSData dataWithBytes:"\x01\x02\x03" length:3];
    BOOL result = [data isEqualToLength:3 bytes:'\x01', '\x02', '\x03'];
    XCTAssertTrue(result);

    XCTAssertTrue([data isEqualToBytes:"\x01\x02\x03" length:3]);
}

- (void)testFromHexString {
    XCTAssertEqualObjects([NSData dataWithHexString:@"01027cff"], [NSData dataWithBytes:"\x01\x02\x7c\xff" length:4]);
    XCTAssertNil([NSData dataWithHexString:@"01027cfg"]);
}

- (void)testFromByteArray {
    XCTAssertEqualObjects(([NSData dataWithByteArray:@[@1, @2, @127, @255]]),
                          [NSData dataWithBytes:"\x01\x02\x7f\xff" length:4]);
    XCTAssertNil(([NSData dataWithByteArray:@[@1, @2, @127, @300]]));
}

@end
