//
//  BDObjCTests.m
//  Benzene Demo
//
//  Created by sodas on 1/25/16.
//  Copyright © 2016 Benzene. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/BenzeneFoundation.h>

@interface BDObjCTests : XCTestCase
@end

@interface BDObjCTestBaseClass : NSObject

@property (nonatomic, strong) NSString *nameA;
@property (nonatomic, strong) NSString *nameB;

@end

@interface BDObjCTestSampleClassA : BDObjCTestBaseClass
@end

@interface BDObjCTestSampleClassB : BDObjCTestBaseClass
@end

@interface BDObjCTestSampleClassC : BDObjCTestSampleClassA
@end

@implementation BDObjCTests

- (void)testSubclassesEnumerator1 {
    XCTAssertEqual(BFClassEnumeratorOfSubclasses(self.class).allObjects.count, 0);
}

- (void)testSubclassesEnumerator2 {
    NSSet *classes = [NSSet setWithArray:BFClassEnumeratorOfSubclasses(BDObjCTestBaseClass.class).allObjects];
    NSSet *expected = [NSSet setWithObjects:BDObjCTestSampleClassA.class,
                                            BDObjCTestSampleClassB.class,
                                            BDObjCTestSampleClassC.class,
                                            nil];
    XCTAssertEqualObjects(classes, expected);
}

- (void)testPropertyEnumeration {
    NSMutableSet *propertyNames = [NSMutableSet setWithCapacity:2];
    BFObjectInspectionEnumerateProperty(BDObjCTestBaseClass.class, ^(objc_property_t property,
                                                                     const char *propertyName,
                                                                     ext_propertyAttributes *propertyAttribute,
                                                                     Class ClassOfProperty,
                                                                     BOOL *stop) {
        if (ClassOfProperty == BDObjCTestBaseClass.class) {
            XCTAssertEqual(propertyAttribute->objectClass, NSString.class);
            [propertyNames addObject:@(propertyName)];
        }
    });

    XCTAssertEqualObjects(propertyNames, ([NSSet setWithObjects:@"nameA", @"nameB", nil]));
}

@end

@implementation BDObjCTestBaseClass
@end

@implementation BDObjCTestSampleClassA
@end

@implementation BDObjCTestSampleClassB
@end

@implementation BDObjCTestSampleClassC
@end
