BENZENE FOUNDATION FRAMEWORK
================================================================================================

A utility and extenstion pack for iOS app development
Compatiable with iOS 5.0+.


HOW TO BUILD
------------------------------------------------------------------------------------------------
Reference : https://github.com/jverkoey/iOS-Framework


DEPENDENCY
------------------------------------------------------------------------------------------------
- Foundation.framework
- UIKit.framework
- CoreGraphics.framework
- QuartzCore.framework
- BenzeneFoundation.strings (optional, used for localization, check ```Localization.md``` to get key list)

HOW TO USE - Use as a standalone framework
------------------------------------------------------------------------------------------------
### 1. Build with "BF-Framework" scheme

![](https://bitbucket.org/wantoto/wantotokit/raw/master/docs-assets/install/Build-as-BF-Framework.png)

This scheme builds a BenzeneFoundation.framework and a BenzeneFoundation.bundle for you.

### 2. Add the framework and the bundle to your project

![](https://bitbucket.org/wantoto/wantotokit/raw/master/docs-assets/install/Products.png)

The output bundle (BenzeneFoundation.bundle) is here. 
You can use Finder to locate the bundle file. The output framework (BenzeneFoundation.framework) is also there.

![](https://bitbucket.org/wantoto/wantotokit/raw/master/docs-assets/install/Products-dir.png)


### 3. Set the build settings
- Add "-ObjC" in "Other Linker Flag" of your "Build Settings"
- Make sure that "BenzeneFoundation.bundle" is in "Copy Bundle Resources" of your "Build Phases"


HOW TO USE - Use as a shared project in the same Xcode Workspace
------------------------------------------------------------------------------------------------
### 1. Add "BenzeneFoundation" Project to your Xcode workspace
After add BenzeneFoundation project to the workspace, build it first (with "BF-Framework" scheme).

![](https://bitbucket.org/wantoto/wantotokit/raw/master/docs-assets/install/Build-as-BF-Framework.png)


### 2. Add the lib and the bundle to your project

![](https://bitbucket.org/wantoto/wantotokit/raw/master/docs-assets/install/Products.png)

- Drag "libBenzeneFoundation.a" to "Link Binary with Libraries" in your "Build Phases"
- Drag "BenzeneFoundation.bundle" to "Copy Bundle Resources" of your "Build Phases"

### 3. Set the path of shared components
- Select "libBenzeneFoundation.a" and open the "File Inspector" in "Utilities" panel.
  Change the "Location" type to "Releative to Group". Make sure the result is like following screenshot.
  You may have to assign the path by yourself.

![](https://bitbucket.org/wantoto/wantotokit/raw/master/docs-assets/install/Relative-path.png)

- Do the samething for "BenzeneFoundation.bundle"

### 4. Set the build settings
- Add "-ObjC" in "Other Linker Flag" of your "Build Settings"
- Make sure that "BenzeneFoundation.bundle" is in "Copy Bundle Resources" of your "Build Phases"
- In the "Build Settings" of your project, fix path in "Library Search Path".
  By draging the libBenzeneFoundation.a into your build phases, Xcode adds a path to the setting.
  But the path is depened on your Mac which leads this project cannot build across other Macs.
  You should change it to ```$(BUILT_PRODUCTS_DIR)```
